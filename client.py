#!/usr/bin/env python
# coding: utf-8

import socket

hote = "localhost"
port = 1500
BUFFER_SIZE = 1024
ClientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
ClientSocket.connect((hote, port))
print ("Connection on {}".format(port))

while True:

	try:
		s = ClientSocket.recv(BUFFER_SIZE)
		print(s.decode('utf-8'))
		response=input()
		if response == "exit":
			break
		else:
			ClientSocket.send(response.encode('utf-8'))
	except KeyboardInterrupt:
		break


ClientSocket.close();
































