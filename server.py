import time
import socket
#from threading import Thread 
#from socketserver import ThreadingMixIn
from threading import Thread 
from socketserver import ThreadingMixIn 
import json
import csv

def load_json_file(file):
        with open(file, 'r') as f:
            json_keys = json.load(f)
        f.close()
        return json_keys
def update_json_file(json_keys,file):
    #lock
    f = open(file, 'w')
    json.dump(json_keys,f, indent=4)   
    f.close()
    #unlock

class ClientThread(Thread):
    """def __init__(self, ip_address, port): 
        #Thread.__init__(self)
        self.ip_address = ip_address
        self.port = port
        self.tcpServer = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.tcpServer.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.tcpServer.bind((self.ip_address, self.port))
        print ("[+] New server socket thread started for " + ip_address + ":" + str(port)) 
    """
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.tcpServer = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.tcpServer.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.tcpServer.bind((self.host, self.port))

    def listen(self):
        self.tcpServer.listen(5)
        while True:
            client, address = self.tcpServer.accept()
            client.settimeout(60)
            Thread(target = self.clientListener,args = (client,address)).start()


    




    def histoAdd(self, client_id, product_ref, product_qty, state):
        #https://docs.python.org/3/library/csv.html
        with open('histo.csv', 'a', newline='') as csvfile:
            orderwriter = csv.writer(csvfile, delimiter='\t',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
            orderwriter.writerow([client_id, product_ref, product_qty, state])
    def viewStock(self, client):
        json_keys=load_json_file("stock.json")
        msg = "---------------------\n"
        for key,value in json_keys.items():
            msg = msg + "Product Reference : " + key +"\t" + "Product Price : " + str(json_keys[key]["price"]) + "\t" + "Product Remaining Stock" + "\t" + str(json_keys[key]["stock"]) + "\n"
        msg = msg+"++++++++++++++++++++\n"
        client.send(msg.encode('utf-8'))

    def viewBills(self, client):
        json_keys=load_json_file("facture.json")
        msg = "---------------------\n"
        for key,value in json_keys.items():
            msg = msg + "Client ID : " + key +"\t" + "Amount To Pay : " + str(json_keys[key])+ "\n"
        msg = msg+"++++++++++++++++++++\n"
        client.send(msg.encode('utf-8'))

    def viewBill(self, client,BUFFER_SIZE):
        client.send("---------------------\nWhat's Your ID ?\n++++++++++++++++++++\n".encode('utf-8'))
        data = client.recv(BUFFER_SIZE)
        key = data.decode('utf-8')
        json_keys=load_json_file("facture.json")
        msg = "---------------------\nClient ID : " + key +"\t" + "Amount To Pay : " + str(json_keys[key])+ "\n++++++++++++++++++++\n"
        client.send(msg.encode('utf-8'))

    def viewHistory(self, client):
        msg = "---------------------\n"
        with open('histo.csv', newline='') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
            for row in spamreader:
                msg= msg +'\n '.join(row) + '\n'
        msg = msg+"++++++++++++++++++++\n"
        client.send(msg.encode('utf-8'))
    def orderTest(self,client, orderItems, json_keys):
        msg = ""
        try:

            if int(orderItems[0]) not in range(1, 5):
                
                msg = msg + "* - Client ID Not Found\n"
        except IndexError:
            msg = msg + "* - It looks like you've forgotten the First attribute \n"
        try:
            if int(orderItems[1]) not in range(1, 5):
                msg = "* - Product Reference Error \n"
        except IndexError:
                msg = msg + "* - It looks like you've forgotten the Second attribute \n"
            
             
        try:
            if int(orderItems[2]) < 1:
                msg = msg +"* - Wrong Quantity Value\n"
                if int(orderItems[2]) > json_keys[orderItems[1]]["stock"]:
                    msg = msg + "* - Stock is not Enough\n"
        except IndexError:
            msg = msg + "* - It looks like you've forgotten the Third attribute \n"

                

        if msg == "":
            client.send("Your Order is correct, do you want to confirm ?, Type y or (Y) ".encode('utf-8'))
            return(True)
        else:
            client.send((msg+"\n ==> Please Pay Attention...\nPress any key to return ... ?, Type y or (Y)\n ").encode('utf-8'))
            return(False)

    def orderCommit(self, orderItems):
        try:
            done = True
            #try 3al les fichiers
            #History Update

            #history.csv
            self.histoAdd(orderItems[0], orderItems[1],orderItems[2],"success")
            
            #stock.json
            json_keys = load_json_file("stock.json")
            price = json_keys[orderItems[1]]["stock"]
            json_keys[orderItems[1]]["stock"] = json_keys[orderItems[1]]["stock"] - int(orderItems[2])

            update_json_file(json_keys, "stock.json")
            #facture.json
            json_keys = load_json_file("facture.json")
            json_keys[orderItems[0]] = json_keys[orderItems[0]] + (price * int(orderItems[2]))
            update_json_file(json_keys, "facture.json")
        
        except IOError as err:
            print("I/O error: {0}".format(err))
            done = False

        return done
 
    def receiveOrder(self, client, BUFFER_SIZE):
        
        data = client.recv(BUFFER_SIZE)
        order = data.decode('utf-8')
        if order:
            response = order
        orderItems = order.split(",")
        print ("  Client {} is making an order:".format(orderItems[0]))
            
        json_keys = load_json_file("stock.json")

        if self.orderTest(client, orderItems, json_keys) == False:
            try:
                self.histoAdd(orderItems[0], orderItems[1],orderItems[2],"failure")
            except IndexError:
                print("Request is Not Complete ")
            response = client.recv(BUFFER_SIZE)
            if (response.decode("utf-8").upper()) == "Y":
                msg = "Order was canceled\nYou'll Be Redirected to Menu Immediately\nPress any key to Continue ...\n"
                client.send(msg.encode('utf-8'))
                
        else:
            # Message de réaffichage de la commande demande de confirmation
            # Lecture de la réponse
            # 
            response = client.recv(BUFFER_SIZE)
            if (response.decode("utf-8").upper()) == "Y":
                if(self.orderCommit(orderItems)):
                    json_keys = load_json_file("facture.json")

                    msg = "Your order was validated successsfully , you have to pay : {} ".format(json_keys[orderItems[0]])
                    client.send(msg.encode('utf-8'))
                    print("Request  Completed successsfully ")
                else:
                    msg = "Order aborted due to some Technical Problems"
                    client.send(msg.encode('utf-8'))
                    print("Request was aborted ")
            else:
                msg = "Order was canceled"
                client.send(msg.encode('utf-8'))
                
                # Retourner à la saisie de commande ou menu Client
                

    def menu (self, client, BUFFER_SIZE):
        msg = "\n++++++++++++++++++++\n1-admin \n2-client\n3-exit\n---------------------\n"

        client.send(msg.encode('utf-8'))
        
        data = client.recv(BUFFER_SIZE)
        choice = data.decode('utf-8')
        if int(choice) == 1 :
            msg = "1-view stock \n2-view bills\n3-view history"
            client.send(msg.encode('utf-8'))
            response = client.recv(BUFFER_SIZE).decode('utf-8')
            if int(response) == 1 :
                self.viewStock(client)
            elif int(response) == 2 :
                self.viewBills(client)
            elif int(response) == 3:
                self.viewHistory(client)
                pass
            else:
                wrong_msg = " WRONG CHOICE , Please rechoose :D "
                client.send(wrong_msg.encode('utf-8'))
                data = client.recv(BUFFER_SIZE)
                choice = data.decode('utf-8')
        elif int(choice) == 2 :
            msg = "++++++++++++++++++++\n1-buy product \n2-get bill\n---------------------\n"
            client.send(msg.encode('utf-8'))
            data = client.recv(BUFFER_SIZE)
            choice = data.decode('utf-8')
            if int(choice) == 1:
                client.send("---------------------\nplease fill in your order in this form [Your ID,Product Reference,Quantity]\n++++++++++++++++++++\n".encode('utf-8'))
                self.receiveOrder(client, BUFFER_SIZE)
                #data = client.recv(BUFFER_SIZE)
                #text = data.decode('utf-8')
            elif int(choice) == 2 :
                self.viewBill(client, BUFFER_SIZE)
        elif int(choice) == 3 :
            self.tcpServer.close()
            print("Host Disconnected successsfully ...")
            return False
        return True

    def clientListener(self, client, address):
        test = True
        BUFFER_SIZE = 1024

        
        while test:
            try:
                test = self.menu(client,BUFFER_SIZE)
            except KeyboardInterrupt:
                self.tcpServer.close()
                break
            
            #To Be EditedMESSAGE = "You ordered {} as a quantity for the product {}, you have to pay : \n Do you confirm this command ?"

            
            #client.send


if __name__ == "__main__":
    while True:
        port_str = input("Port? ")
        try:
            port = int(port_str)
            break
        except ValueError:
            pass
    host = input("Server Address ?")
    ClientThread(host,port).listen()

